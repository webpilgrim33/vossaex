# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Candidate'
        db.create_table(u'cand_candidate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('candidate_sequential_nr', self.gf('django.db.models.fields.BigIntegerField')(db_index=True, null=True, blank=True)),
            ('election_year', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('round_nr', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('election_description', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=2, null=True, blank=True)),
            ('electoral_unity', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('electoral_unity_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('post_code', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('post_description', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('candidate_name', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('candidate_nr', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('candidate_name_box', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('candidacy_status_code', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('candidacy_status_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('party_nr', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('party_acronym', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=10, null=True, blank=True)),
            ('party_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('label_code', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('label_acronym', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('label_composition', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('label_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('profession_code', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('profession_desc', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('birth_date', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('candidate_electoral_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('age_election_date', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('gender_code', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('gender_description', self.gf('django.db.models.fields.CharField')(max_length=9, null=True, blank=True)),
            ('scholarity_code', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('scholarity_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('civil_status_code', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('civil_status_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('race_color_code', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('race_color_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('nationality_code', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('nationality_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('state_birth', self.gf('django.db.models.fields.CharField')(max_length=2, null=True, blank=True)),
            ('city_birth_code', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('city_birth_name', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('campaing_max_expense', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=2, blank=True)),
            ('candidate_status_code', self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True)),
            ('candidate_status_desc', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('gen_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('gen_hour', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('patrimony', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=14, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'cand', ['Candidate'])

        # Adding index on 'Candidate', fields ['election_year', 'post_code', 'state', 'party_acronym']
        db.create_index(u'cand_candidate', ['election_year', 'post_code', 'state', 'party_acronym'])

        # Adding index on 'Candidate', fields ['candidate_sequential_nr', 'election_year']
        db.create_index(u'cand_candidate', ['candidate_sequential_nr', 'election_year'])

        # Adding model 'Property'
        db.create_table(u'cand_property', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('candidate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cand.Candidate'], null=True)),
            ('candidate_sequential_nr', self.gf('django.db.models.fields.BigIntegerField')(db_index=True, null=True, blank=True)),
            ('election_year', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('election_description', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=2, null=True, blank=True)),
            ('property_type_code', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('property_type_desc', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('property_desc', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('property_value', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=2, blank=True)),
            ('gen_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('gen_hour', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('last_update_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('last_update_hour', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'cand', ['Property'])

        # Adding index on 'Property', fields ['candidate_sequential_nr', 'election_year']
        db.create_index(u'cand_property', ['candidate_sequential_nr', 'election_year'])


    def backwards(self, orm):
        # Removing index on 'Property', fields ['candidate_sequential_nr', 'election_year']
        db.delete_index(u'cand_property', ['candidate_sequential_nr', 'election_year'])

        # Removing index on 'Candidate', fields ['candidate_sequential_nr', 'election_year']
        db.delete_index(u'cand_candidate', ['candidate_sequential_nr', 'election_year'])

        # Removing index on 'Candidate', fields ['election_year', 'post_code', 'state', 'party_acronym']
        db.delete_index(u'cand_candidate', ['election_year', 'post_code', 'state', 'party_acronym'])

        # Deleting model 'Candidate'
        db.delete_table(u'cand_candidate')

        # Deleting model 'Property'
        db.delete_table(u'cand_property')


    models = {
        u'cand.candidate': {
            'Meta': {'object_name': 'Candidate', 'index_together': "[['election_year', 'post_code', 'state', 'party_acronym'], ['candidate_sequential_nr', 'election_year']]"},
            'age_election_date': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'birth_date': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'campaing_max_expense': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'}),
            'candidacy_status_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'candidacy_status_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'candidate_electoral_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'candidate_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'candidate_name_box': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'candidate_nr': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'candidate_sequential_nr': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'candidate_status_code': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'candidate_status_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'city_birth_code': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'city_birth_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'civil_status_code': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'civil_status_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'election_description': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'election_year': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'electoral_unity': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'electoral_unity_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'gen_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'gen_hour': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'gender_code': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'gender_description': ('django.db.models.fields.CharField', [], {'max_length': '9', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label_acronym': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'label_code': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'label_composition': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'label_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nationality_code': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'nationality_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'party_acronym': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'party_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'party_nr': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'patrimony': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '14', 'decimal_places': '2', 'blank': 'True'}),
            'post_code': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'post_description': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'profession_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'profession_desc': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'race_color_code': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'race_color_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'round_nr': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'scholarity_code': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'scholarity_desc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'state_birth': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'})
        },
        u'cand.property': {
            'Meta': {'object_name': 'Property', 'index_together': "[['candidate_sequential_nr', 'election_year']]"},
            'candidate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cand.Candidate']", 'null': 'True'}),
            'candidate_sequential_nr': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'election_description': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'election_year': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'gen_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'gen_hour': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_update_hour': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'property_desc': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'property_type_code': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'property_type_desc': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'property_value': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cand']