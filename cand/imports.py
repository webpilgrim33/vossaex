import csv
import datetime
import traceback
from django.db import connection

from .models import Candidate, Property, State, Year, Post, Party


def import_candidates(file):
    """imports to Candidate model from csv file given"""
    rows = csv.DictReader(file)

    created_items = 0

    for r in rows:
        try:
            c = Candidate()
            c.candidate_sequential_nr=long(r['candidate_sequential_nr'])
            c.gen_date = datetime.datetime.strptime(r['gen_date'], '%Y-%m-%d').date()
            c.gen_hour = datetime.datetime.strptime(r['gen_hour'], '%H:%M:%S').time()
            c.election_year = int(r['election_year'])
            c.round_nr = r['round_nr']
            c.election_description = r['election_description']
            c.state = r['state']
            c.electoral_unity = r['electoral_unity']
            c.electoral_unity_desc = r['electoral_unity_desc']
            c.post_code = int(r['post_code'])
            c.post_description = r['post_description']
            c.candidate_name = r['candidate_name']
            c.candidate_nr = int(r['candidate_nr'])
            c.candidate_name_box = r['candidate_name_box']
            c.candidacy_status_code = int(r['candidacy_status_code'])
            c.candidacy_status_desc = r['candidacy_status_desc']
            c.party_nr = int(r['party_nr'])
            c.party_acronym = r['party_acronym']
            c.party_name = r['party_name']
            c.label_code = int(r['label_code'])
            c.label_acronym = r['label_acronym']
            c.label_composition = r['label_composition']
            c.label_name = r['label_name']
            c.profession_code = int(r['profession_code'])
            c.profession_desc = r['profession_desc']
            c.birth_date = r['birth_date']
            c.candidate_electoral_id = long(r['candidate_electoral_id'])
            c.age_election_date = r['age_election_date'] 
            c.gender_code = (r['gender_code'])
            c.gender_description = r['gender_description']
            c.scholarity_code = r['scholarity_code'] 
            c.scholarity_desc = r['scholarity_desc'] 
            c.civil_status_code = r['civil_status_code']
            c.civil_status_desc = r['civil_status_desc']
            c.race_color_code = r['race_color_code']
            c.race_color_desc = r['race_color_desc'] 
            c.nationality_code = r['nationality_code']
            c.nationality_desc = r['nationality_desc']
            c.state_birth = r['state_birth']
            c.city_birth_code = r['city_birth_code'] 
            c.city_birth_name = r['city_birth_name'] 
            c.campaing_max_expense = r['campaing_max_expense'] 
            c.candidate_status_code = r['candidate_status_code']
            c.candidate_status_desc = r['candidate_status_desc']
            c.save()
            created_items += 1
        except Exception, e:
            tb = traceback.format_exc()
            print 'Error %s importing row:\n %s' % (str(e), str( r))
            print tb
    update_cache()
    return created_items

def import_properties(file):
    """imports to Property model from csv file given"""
    rows = csv.DictReader(file)

    created_items = 0

    for r in rows:
        try:
            p = Property()
            p.candidate = Candidate.objects.filter(
                candidate_sequential_nr=long(r['candidate_sequential_nr']),
                election_year=int(r['election_year']),
                state=r['state'],
                round_nr=1
                )[0]
            p.candidate_sequential_nr = long(r['candidate_sequential_nr'])
            p.election_year = int(r['election_year'])
            p.election_description = r['election_description']
            p.state = r['state']
            p.property_type_code = int(r['property_type_code'])
            p.property_type_desc = r['property_type_desc']
            p.property_desc = r['property_desc']
            p.property_value = r['property_value']
            p.gen_date = datetime.datetime.strptime(r['gen_date'], '%Y-%m-%d').date()
            p.gen_hour = datetime.datetime.strptime(r['gen_hour'], '%H:%M:%S').time()
            p.last_update_date = datetime.datetime.strptime(r['last_update_date'], '%Y-%m-%d').date()
            p.last_update_hour =\
            datetime.datetime.strptime(r['last_update_hour'], '%H:%M:%S').time()
            p.save()
            created_items += 1
        except Exception, e:
            tb = traceback.format_exc()
            print 'Error %s importing row:\n %s' % (str(e), str( r))
            print tb

    calc_patrimony()
    return created_items

def calc_patrimony():
    cursor = connection.cursor()
    cursor.execute(
        """update cand_candidate c set patrimony=coalesce(
           (select sum(property_value) from cand_property p where
           candidate_id=c.id), 0)"""
        )

def update_cache():
    cursor = connection.cursor()
    cursor.execute(
        'select distinct election_year::Integer from cand_candidate '
        )
    for row in cursor.fetchall():
        o, _ = Year.objects.get_or_create(year=row[0])
        o.save()

    cursor.execute(
        'select distinct state from cand_candidate '
        )
    for row in cursor.fetchall():
        o, _ = State.objects.get_or_create(code=row[0])
        o.save()

    cursor.execute(
        'select distinct post_code, post_description from cand_candidate '
        )
    for row in cursor.fetchall():
        o, _ = Post.objects.get_or_create(code=row[0], desc=row[1])
        o.save()

    cursor.execute(
        'select distinct party_acronym, party_name from cand_candidate '
         )
    for row in cursor.fetchall():
        o, _ = Party.objects.get_or_create(party_acronym=row[0], party_name=row[1])
        o.save()
 
