from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection
from django.db.models import Q, Sum
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView

from django.db.models import aggregates
from django.db.models.sql import aggregates as sql_aggregates

from models import Candidate, Property, State, Year, Post, Party

class Variation(TemplateView):
    template_name = 'cand/patrimonial_variation.html'

    def get_context_data(self, **kwargs):
        context = super(Variation, self).get_context_data(**kwargs)
        results = list(self.get_queryset())

            
        #get filter
        context['filter'] = self.filter
        context['initial_year'] = self.initial_year
        context['final_year'] = self.final_year
        context['page_number'] = self.page + 1
        context['previous_page'] = self.page - 1
        context['next_page'] = self.page + 1
        context['page_size'] = self.page_size
        context['results'] = results[0:self.page_size]
        if len(results) > self.page_size:
            context['has_next_page'] = True
        context['query_string'] = self.request.META['QUERY_STRING']
        
        return context

    def get_queryset(self):
        self.filter = {}
        self.filter['years'] = Year.objects.all().order_by('-year')
        self.filter['states'] = State.objects.all().order_by('code')
        self.filter['posts'] = Post.objects.all().order_by('desc')
        self.initial_year = self.request.GET.get('initial_year', str(self.filter['years'][1].year))
        self.final_year = self.request.GET.get('final_year', str(self.filter['years'][0].year))
        self.page_size = int(self.request.GET.get('row_count', 10))
        self.page = int(self.request.GET.get('page', 0))
        limit = self.page_size + 1
        offset = self.page_size * self.page
        sql = """
select row_number() OVER(ORDER BY variation DESC) AS position, * 
from (
  select  c.*, initial.election_year, initial.sum as initial_sum, final.election_year, 
        final.sum as final_sum,
        case when initial.sum >= 1 then final.sum/initial.sum::Float else 0 end
        as variation 
  from cand_candidate c join 
   ( 
    select c.candidate_electoral_id, 
     c.election_year, patrimony as sum
    from cand_candidate c 
    where c.election_year=%s 
   ) final
   on final.candidate_electoral_id=c.candidate_electoral_id
      and final.election_year=c.election_year 
   join
   ( 
    select c.candidate_electoral_id, 
      c.election_year, patrimony as sum
    from cand_candidate c 
    where c.election_year=%s"""
        if self.request.GET.get('elected_only') is not None:
            sql += "and c.candidate_status_desc like 'ELEITO%%' "
        sql +="""
   ) initial
   on final.candidate_electoral_id=initial.candidate_electoral_id
    where 1=1
   """
        posts = ["'%d'" % int(x) for x in self.request.GET.getlist('post[]')]
        if len(posts) > 0:
            sql += 'and c.post_code in (%s) ' % (', '.join(posts))

        states = ["'%s'" % x for x in self.request.GET.getlist('state[]')]
        if len(states) > 0:
            sql += 'and c.state in (%s) ' % (', '.join(states))

        sql += """
) t 
order by variation desc
limit %s offset %s
"""
        
        results = Candidate.objects.raw(sql,
            (self.final_year, self.initial_year,
             limit, offset)
            )
        return results

class CandidateDetail(TemplateView):
    template_name = 'cand/candidate_detail.html'

    def get_context_data(self, e_id):
        context = super(CandidateDetail, self).get_context_data()
        queryset = Candidate.objects.filter(
            candidate_electoral_id=e_id
            ).order_by('election_year')
        max_value = max([x.get_property_total() for x in queryset])
        if max_value > 0:
            chart_data = [(x.election_year, x.patrimony,
            int(x.patrimony * 100 / max_value)) for x in queryset]
            context['chart'] = chart_data
            context['labels'] = [ (5 - x) * int(max_value / 5) for x in range(5)]
        context['candidate'] = queryset
        
        return context

class Search(TemplateView):
    template_name = 'cand/search.html'
    
    def get_context_data(self):
        context = super(Search, self).get_context_data()
        search_type = self.request.GET.get('search_type', 'c')
        query = self.request.GET.get('q', None)
        context['search_type'] = search_type
        context['query'] = query

        self.filter = self.__get_filter()
        
        if query is None:
            results = None
        elif search_type == 'c':
            results = self.__candidates(query)
        elif search_type == 'p':
            results = self.__properties(query)

        if query is not None:
            context['show_results'] = True

        if results is not None:
            page = self.request.GET.get('page')
            paginator = Paginator(results, 30)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context['results'] = results
        context['query_string'] = self.request.META['QUERY_STRING']
        context['filter'] = self.filter
        return context

    def __candidates(self, query):
        filter_cond = []
        if query != '':
            filter_cond.append(Q(candidate_name__icontains=query) |
            Q(candidate_name_box__icontains=query))
        years = self.request.GET.getlist('year[]')
        if len(years) > 0:
            filter_cond.append(Q(election_year__in=years))
        party = self.request.GET.getlist('party[]')
        if len(party) > 0:
            filter_cond.append(Q(party_acronym__in=party))
        state = self.request.GET.getlist('state[]')
        if len(state) > 0:
            filter_cond.append(Q(state__in=state))
        post = self.request.GET.getlist('post[]')
        if len(post) > 0:
            filter_cond.append(Q(post_code__in=post))

        # if len(filter_cond) == 0:
        #     return None

        qs = Candidate.objects.filter(
            *filter_cond
            )


        range = {
            '0': [],
            '1': [Q(patrimony__gte=0, patrimony__lte=10000)],
            '2': [Q(patrimony__gte=10000, patrimony__lte=100000)],
            '3': [Q(patrimony__gte=100000, patrimony__lte=1000000)],
            '4': [Q(patrimony__gte=1000000, patrimony__lte=10000000)],
            '5': [Q(patrimony__gte=10000000)],
            }

        qs = qs.filter(*(range[self.request.GET.get('patrimony', '0')]))
        sort = self.request.GET.get('sort', 'desc')
        qs = qs.order_by(
            {'asc': 'patrimony',
             'desc': '-patrimony'}[sort]
             )

        return qs        


    def __properties(self, query):
        filter_cond = []
        if query != '':
            filter_cond.append(Q(property_desc__icontains=query) |
            Q(property_type_desc__icontains=query))
        years = self.request.GET.getlist('year[]')
        if len(years) > 0:
            filter_cond.append(Q(election_year__in=years))
        party = self.request.GET.getlist('party[]')
        if len(party) > 0:
            filter_cond.append(Q(candidate_sequential_nr__party_acronym__in=party))
        state = self.request.GET.getlist('state[]')
        if len(state) > 0:
            filter_cond.append(Q(state__in=state))
        post = self.request.GET.getlist('post[]')
        if len(post) > 0:
            filter_cond.append(Q(candidate_sequential_nr__post_code__in=post))
        ptype = self.request.GET.getlist('ptype[]')
        if len(ptype) > 0:
            filter_cond.append(Q(property_type_code__in=ptype))

        # if len(filter_cond) == 0:
        #     return None
            
        qs = Property.objects.filter(
            *filter_cond
            )
        
        range = {
            '0': [],
            '1': [Q(property_value__gte=0, property_value__lte=10000)],
            '2': [Q(property_value__gte=10000, property_value__lte=100000)],
            '3': [Q(property_value__gte=100000, property_value__lte=1000000)],
            '4': [Q(property_value__gte=1000000, property_value__lte=10000000)],
            '5': [Q(property_value__gte=10000000)],
            }
            
        qs = qs.filter(*(range[self.request.GET.get('patrimony', '0')]))
        sort = self.request.GET.get('sort', 'desc')
        qs = qs.order_by(
            {'asc': 'property_value',
             'desc': '-property_value'}[sort]
             )
        return qs
        
    def __get_filter(self):
        filter = {}
        filter['years'] = Year.objects.all().order_by('-year')
        filter['states'] = State.objects.all().order_by('code')
        filter['posts'] = Post.objects.all().order_by('desc')
        filter['party'] = Party.objects.all().order_by('party_acronym')
        return filter
