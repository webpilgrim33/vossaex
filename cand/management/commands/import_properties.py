from django.core.management.base import BaseCommand

from cand.imports import calc_patrimony, import_properties


class Command(BaseCommand):
    help = 'usage ./manage.py import_candidates filename.csv'

    def handle(self, *args, **options):
        if len(args) > 0:
            afile = open(args[0], 'r')
            res = import_properties(afile)
            calc_patrimony()
            print 'Import finished. Created %d' % res
        else:
            print help

