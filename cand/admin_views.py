from django import forms
from django.views.generic import View
from django.shortcuts import render

from imports import import_candidates, import_properties

class UploadFileForm(forms.Form):
    file = forms.FileField()

class ImportCandidateView(View):
    def post(self, request):
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            res = import_candidates(request.FILES['file'])
            return render(request, 'cand/admin/choose_file.html', {'res': res})
        else:
            return render(request, 'cand/admin/choose_file.html', {'form':form})

    def get(self, request):
        form = UploadFileForm()
        return render(request, 'cand/admin/choose_file.html', {'form':form})

class ImportPropertiesView(View):
    def post(self, request):
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            res = import_properties(request.FILES['file'])
            return render(request, 'cand/admin/choose_file.html', {'res': res})
        else:
            return render(request, 'cand/admin/choose_file.html', {'form':form})

    def get(self, request):
        form = UploadFileForm()
        return render(request, 'cand/admin/choose_file.html', {'form':form})
