from django.db import models
from django.db.models import Sum

class Candidate(models.Model):
    candidate_sequential_nr = models.BigIntegerField(null=True, blank=True, db_index=True)    
    election_year = models.IntegerField(blank=True, null=True, db_index=True)
    round_nr = models.CharField(max_length=1, blank=True, null=True) # Some elections may have 2 rounds
    election_description = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True, db_index=True) # Acronym for the state
    electoral_unity = models.CharField(max_length=50, blank=True, null=True)
    # Electoral unity description
    electoral_unity_desc = models.CharField(max_length=50, blank=True, null=True) 
    post_code = models.IntegerField(blank=True, null=True, db_index=True)
    post_description = models.CharField(max_length=50, blank=True, null=True)
    candidate_name = models.CharField(max_length=200, blank=True, null=True)
    candidate_nr = models.IntegerField(blank=True, null=True)
    # Name in the ballot box
    candidate_name_box = models.CharField(max_length=200, blank=True, null=True) 
    candidacy_status_code = models.IntegerField(blank=True, null=True)
    candidacy_status_desc = models.CharField(max_length=50, blank=True, null=True)
    party_nr = models.IntegerField(blank=True, null=True)
    party_acronym = models.CharField(max_length=10, blank=True, null=True, db_index=True)
    party_name = models.CharField(max_length=100, blank=True, null=True)
    label_code = models.BigIntegerField(blank=True, null=True)
    label_acronym = models.CharField(max_length=10, blank=True, null=True)
    label_composition = models.CharField(max_length=150, blank=True, null=True)
    label_name = models.CharField(max_length=100, blank=True, null=True)
    profession_code = models.IntegerField(blank=True, null=True)
    profession_desc = models.CharField(max_length=200, blank=True, null=True)
    birth_date = models.CharField(max_length=10, blank=True, null=True)
    candidate_electoral_id = models.BigIntegerField(blank=True, null=True)
    age_election_date = models.CharField(max_length=10, blank=True, null=True)
    gender_code = models.CharField(max_length=1, blank=True, null=True)
    gender_description = models.CharField(max_length=9, blank=True, null=True)
    scholarity_code = models.CharField(max_length=4, blank=True, null=True)
    scholarity_desc = models.CharField(max_length=50, blank=True, null=True)
    civil_status_code = models.CharField(max_length=4, blank=True, null=True)
    civil_status_desc = models.CharField(max_length=50, blank=True, null=True)
    race_color_code = models.CharField(max_length=4, blank=True, null=True)
    race_color_desc = models.CharField(max_length=50, blank=True, null=True)
    nationality_code = models.CharField(max_length=4, blank=True, null=True)
    nationality_desc = models.CharField(max_length=50, blank=True, null=True)
    state_birth = models.CharField(max_length=2, blank=True, null=True)
    city_birth_code = models.CharField(max_length=5, blank=True, null=True)
    city_birth_name = models.CharField(max_length=200, blank=True, null=True)
    campaing_max_expense = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # If candidate was elected
    candidate_status_code = models.CharField(max_length=3, blank=True, null=True) 
    candidate_status_desc = models.CharField(max_length=50, blank=True, null=True)
    gen_date = models.DateField(blank=True, null=True) # date of data generation
    gen_hour = models.TimeField(blank=True, null=True) # hour of data generation
    patrimony = models.DecimalField(max_digits=14, decimal_places=2, blank=True,
        null=False, default=0, db_index=True)

    def get_property(self):
        return Property.objects.filter(
            candidate=self
            ).order_by('property_desc')

    def get_property_total(self): 
        amount = Property.objects.filter(
            candidate=self
            ).aggregate(total=Sum('property_value'))
        res = amount.get('total')
        if res is None:
            res = 0
        return res
    
    def __unicode__(self):
        return '%d: %s' % (self.candidate_sequential_nr, self.candidate_name)

    class Meta():
        index_together = [
            ["election_year", "post_code", "state", "party_acronym"],
            ['candidate_sequential_nr', 'election_year', 'state']
        ]
    
class Property(models.Model):
    candidate = models.ForeignKey('Candidate', null=True) 
    candidate_sequential_nr = models.BigIntegerField(null=True, blank=True, db_index=True)  
    election_year = models.IntegerField(blank=True, null=True, db_index=True)
    election_description = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    property_type_code = models.IntegerField(blank=True, null=True, db_index=True)
    property_type_desc = models.CharField(max_length=200, blank=True, null=True)
    property_desc = models.CharField(max_length=200, blank=True, null=True)    
    property_value = models.DecimalField(max_digits=14, decimal_places=2, blank=True, null=True)
    gen_date = models.DateField(blank=True, null=True)
    gen_hour = models.TimeField(blank=True, null=True)
    last_update_date = models.DateField(blank=True, null=True)
    last_update_hour = models.TimeField(blank=True, null=True)

    class Meta():
        verbose_name_plural = "properties"
        index_together = [
            ['candidate_sequential_nr', 'election_year']
        ]


class State(models.Model):
    """
    Model to store cached states for fast creation of filters
    """
    code = models.CharField(max_length=2, blank=False, null=False)

class Year(models.Model):
    """
    Model to store cached years for fast creation of filters
    """
    year = models.IntegerField(blank=False, null=False)

class Post(models.Model):
    """
    Model to store cached posts for fast creation of filters
    """
    code = models.IntegerField(blank=False, null=False)
    desc = models.CharField(max_length=50, blank=True, null=True)

class Party(models.Model):
    """
    Model to store cached parties for fast creation of filters
    """
    party_acronym = models.CharField(max_length=10, blank=True, null=True, db_index=True)
    party_name = models.CharField(max_length=100, blank=True, null=True)
