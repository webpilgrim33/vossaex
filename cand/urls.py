from __future__ import unicode_literals

from .views import Variation, CandidateDetail, Search
from .admin_views import ImportCandidateView, ImportPropertiesView
from django.conf.urls import patterns, url


urlpatterns = patterns('cand.views',
    url(r'^$', Variation.as_view(), name='patrimony_variation'),
    url(r'^candidate/(?P<e_id>\d+)/$', CandidateDetail.as_view(),
        name='candidate'),
    url(r'^search/$', Search.as_view(), name='search'),
    url(r'import_cadidates/', ImportCandidateView.as_view(),
        name='import_candidates'),
    url(r'import_properties/', ImportPropertiesView.as_view(),
        name='import_properties')
    
    # url(r'^/graphs/$', GraphOverview.as_view(), name='workout_graphs'),
    # url(r'^/importfile/$', ImportFitView.as_view(), name='workout_importfile'),
    # url(r'^/(?P<pk>\d+)', WorkoutDetail.as_view(), name='workout_detail'),
    # url(r'^/masscategories/$', AdminMassCategory.as_view(), name='workout_admin_masscategories'),
)
