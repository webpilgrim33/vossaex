function urlToDict(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        if (pair[0] == '') continue;
        var key = decodeURIComponent(pair[0]) ;
        var value = decodeURIComponent(pair[1]);
        if (key.indexOf('[]') > 0) {
            if (typeof(request[key]) == 'undefined') {
                request[key] = [];
            }
            request[key].push(value);
        } else {
            request[key] = value;
        }
    }
    return request;
};

var filter = function(fid) {
    var form = $(fid);

    var f = {

        init: function() {
            var params = urlToDict(document.location.href);

            f.set_patrimony(params.patrimony);
            f.set_sort(params.sort);

            f.set_posts(params['post[]']);
            f.set_states(params['state[]']);
            f.set_years(params['year[]']);
            f.set_party(params['party[]']);
            f.set_ptypes(params['ptype[]']);

            $('#ptype-all').on('click', function(e, ui) {
                $('#ptype input[name="ptype[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#ptype input[name="ptype[]"]').on('click', function (e, ui) {
                $('#ptype-all').attr('checked', f.get_ptypes().length === 0);
            });

            $('#post-all').on('click', function(e, ui) {
                $('#post input[name="post[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#post input[name="post[]"]').on('click', function (e, ui) {
                $('#post-all').attr('checked', f.get_posts().length === 0);
            });

            $('#state-all').on('click', function(e, ui) {
                $('#state input[name="state[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#state input[name="state[]"]').on('click', function (e, ui) {
                $('#state-all').attr('checked', f.get_states().length === 0);
            });

            $('#party-all').on('click', function(e, ui) {
                $('#party input[name="party[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#party input[name="party[]"]').on('click', function (e, ui) {
                $('#party-all').attr('checked', f.get_party().length === 0);
            });

            $('#year-all').on('click', function(e, ui) {
                $('#year input[name="year[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#year input[name="year[]"]').on('click', function (e, ui) {
                $('#year-all').attr('checked', f.get_years().length === 0);
            });

            
        },

        get_ptypes : function() { 
            var values = new Array();
            $.each($("input[name='ptype[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_ptypes : function(val) {
            if(val === undefined) return;
            form.find('#ptype input[name="ptype[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#ptype-all').attr('checked', false);
                        }
                    }
                });
        },

        get_posts : function() { 
            var values = new Array();
            $.each($("input[name='post[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_posts : function(val) {
            if(val === undefined) return;
            form.find('#post input[name="post[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#post-all').attr('checked', false);
                        }
                    }
                });
        },

        get_states : function() { 
            var values = [];
            $.each($("input[name='state[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_states : function(val) {
            if(val === undefined) return;
            form.find('#state input[name="state[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#state-all').attr('checked', false);
                        }
                    }
                });
        },

        get_years : function() { 
            var values = [];
            $.each($("input[name='year[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_years : function(val) {
            if(val === undefined) return;
            form.find('#year input[name="year[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#year-all').attr('checked', false);
                        }
                    }
                });
        },

        get_party : function() { 
            var values = [];
            $.each($("input[name='party[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_party : function(val) {
            if(val === undefined) return;
            form.find('#party input[name="party[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#party-all').attr('checked', false);
                        }
                    }
                });
        },


        get_patrimony : function() {
            var sel = form.find('#patrimony');
            return sel.val();
        },

        set_patrimony : function(val) {
            var sel = form.find('#patrimony');
            sel.val(val);
        },

        get_sort : function() {
            var sel = form.find('#sort');
            return sel.val();
        },

        set_sort : function(val) {
            var sel = form.find('#sort');
            sel.val(val);
        },

        check_type: function(type_code) {
            if(type_code == 'c') {
                form.find('#vis-patrimony').show();
                form.find('#vis-property-value').hide();
                form.find('#property-type').hide();
            } else if(type_code == 'p') {
                form.find('#vis-patrimony').hide();
                form.find('#vis-property-value').show();
                form.find('#property-type').show();
            }
        },

        get_params: function() {
            return {
                'party[]': f.get_party(),
                'year[]': f.get_years(),
                'post[]': f.get_posts(),
                'state[]': f.get_states(),
                'ptype[]': f.get_ptypes(),
                'patrimony': f.get_patrimony(),
                'sort': f.get_sort()
            };
        }
    };
    return f;
};




var searchform = function(id, flt) {
    var form = $(id);

    var sf = {

        init: function() {
            form.find('#search_type_selector li a').click(sf.selector_click);
            form.find('#submit').click(sf.submit);
            form.find('#query').keypress(function(event) {
                if (event.keyCode == 13) {
                    sf.submit();
                }
            });
            sf.change_type(form.find('#search_type').val());
        },

        selector_click: function() {
            sf.change_type(this.id[this.id.length - 1]);

        },

        get_search_type: function() {
            return form.find('#search_type').val();
        },

        get_query: function() {
            return form.find('#query').val();
        },

        get_params: function() {
            return {
                'q' : sf.get_query(),
                'search_type': sf.get_search_type()
            };
        },

        change_type: function(type_code) {
            if(type_code == 'c') {
                form.find('#search_type').val(type_code);
                form.find('#vis_cand').show();
                form.find('#vis_prop').hide();
            } else if(type_code == 'p') {
                form.find('#search_type').val(type_code);
                form.find('#vis_prop').show();
                form.find('#vis_cand').hide();
            }
            flt.check_type(type_code);
        },

        submit: function() {
            var f_params = $.param(flt.get_params());
            var s_params = $.param(sf.get_params());
            document.location = '?' + s_params + '&' + f_params;
        }
    };
    return sf;
};

