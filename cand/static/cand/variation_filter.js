function urlToDict(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        if (pair[0] == '') continue;
        var key = decodeURIComponent(pair[0]) ;
        var value = decodeURIComponent(pair[1]);
        if (key.indexOf('[]') > 0) {
            if (typeof(request[key]) == 'undefined') {
                request[key] = [];
            }
            request[key].push(value);
        } else {
            request[key] = value;
        }
    }
    return request; 
};


var filter = function(id, years, states, posts) {
    var form = $(id);

    var f = {

        init: function() {
            f.fill_final_year();
            f.fill_initial_year();
            form.find('#final_year').change(f.check_initial_year);
            form.find('#initial_year').change(f.update_cbl_year);

            var params = urlToDict(document.location.href);

            f.set_initial_year(params.initial_year);
            f.set_final_year(params.final_year);

            f.check_initial_year();
            f.set_row_count(params.row_count);
            f.set_elected_only(params.elected_only);
            f.set_posts(params['post[]']);
            f.set_states(params['state[]']);

            $('#post-all').on('click', function(e, ui) {
                $('#post input[name="post[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#post input[name="post[]"]').on('click', function (e, ui) {
                $('#post-all').attr('checked', f.get_posts().length === 0);
            });

            $('#state-all').on('click', function(e, ui) {
                $('#state input[name="state[]"]').prop('checked',!$(this).prop('checked') );
            });
            $('#state input[name="state[]"]').on('click', function (e, ui) {
                $('#state-all').attr('checked', f.get_states().length === 0);
            });

            
        },

        get_posts : function() { 
            var values = new Array();
            $.each($("input[name='post[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_posts : function(val) {
            if(val === undefined) return;
            form.find('#post input[name="post[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#post-all').attr('checked', false);
                        }
                    }
                });
        },

        get_states : function() { 
            var values = [];
            $.each($("input[name='state[]']:checked"), function() {
                values.push($(this).val());
            });
            return values;
        },

        set_states : function(val) {
            if(val === undefined) return;
            form.find('#state input[name="state[]"]').each( 
                function(j, el) {
                    for(var i = 0; i < val.length; i++) {
                        if($(this).val() == val[i]) {
                            $(this).attr('checked', true);
                            $('#state-all').attr('checked', false);
                        }
                    }
                });
        },

        
        fill_final_year : function() {
            var sel = form.find('#final_year');
            sel.children().remove();
            for(var i = 0; i < years.length - 1; i++) {
                sel.append('<option value="' + years[i] +'">' + years[i] + '</option>');
            }
        },

        fill_initial_year : function() {
            var sel = form.find('#initial_year');
            sel.children().remove();
            for(var i = 1; i < years.length; i++) {
                sel.append('<option value="' + years[i] +'">' + years[i] + '</option>');
            }
        },

        get_final_year : function() {
            var sel = form.find('#final_year');
            return sel.val();
        },

        set_final_year : function(year) {
            var sel = form.find('#final_year');
            sel.val(year);
        },

        get_initial_year : function() {
            var sel = form.find('#initial_year');
            return sel.val();
        },

        set_initial_year : function(year) {
            var sel = form.find('#initial_year');
            sel.val(year);
        },

        update_cbl_year: function() {
            form.find('#cbl_year').html(f.get_initial_year());
        },

        get_row_count : function() {
            return form.find('#row_count').val();
        },

        set_row_count : function(row_count) {
            return form.find('#row_count').val(row_count);
        },

        get_elected_only : function() {
            return form.find('#elected_only').attr('checked');
        },

        set_elected_only : function(val) {
            var cb = form.find('#elected_only');
            if(val != undefined)
                cb.attr('checked', true);
            else
                cb.removeAttr('checked');
        },


        check_initial_year : function() {
            var final_year = f.get_final_year();
            if(f.get_initial_year() >= final_year) {
                for(var i = 0; i < years.length; i++) {
                    if(years[i] < final_year) {
                        f.set_initial_year(years[i]);
                        break;
                    }
                }
            }
            var sel = form.find('#initial_year');
            sel.children().each(function() {
                if(this.value >= final_year) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        }

    };
    return f;
};

