from django.conf import settings
from django.template import defaultfilters
from django import template
from django.utils.translation import ungettext


register = template.Library()

# A tuple of standard large number to their converters
intword_converters = (
    (3, lambda number: (
        ungettext('%(value).1f thousand', '%(value).1f thousands', number),
        ungettext('%(value)s thousand', '%(value)s thousands', number),
    )),
    (6, lambda number: (
        ungettext('%(value).1f million', '%(value).1f millions', number),
        ungettext('%(value)s million', '%(value)s millions', number),
    )),
    (9, lambda number: (
        ungettext('%(value).1f billion', '%(value).1f billions', number),
        ungettext('%(value)s billion', '%(value)s billions', number),
    )),
    (12, lambda number: (
        ungettext('%(value).1f trillion', '%(value).1f trillions', number),
        ungettext('%(value)s trillion', '%(value)s trillions', number),
    )),
    (15, lambda number: (
        ungettext('%(value).1f quadrillion', '%(value).1f quadrillion', number),
        ungettext('%(value)s quadrillion', '%(value)s quadrillion', number),
    )),
    (18, lambda number: (
        ungettext('%(value).1f quintillion', '%(value).1f quintillion', number),
        ungettext('%(value)s quintillion', '%(value)s quintillion', number),
    )),
    (21, lambda number: (
        ungettext('%(value).1f sextillion', '%(value).1f sextillion', number),
        ungettext('%(value)s sextillion', '%(value)s sextillion', number),
    )),
    (24, lambda number: (
        ungettext('%(value).1f septillion', '%(value).1f septillion', number),
        ungettext('%(value)s septillion', '%(value)s septillion', number),
    )),
    (27, lambda number: (
        ungettext('%(value).1f octillion', '%(value).1f octillion', number),
        ungettext('%(value)s octillion', '%(value)s octillion', number),
    )),
    (30, lambda number: (
        ungettext('%(value).1f nonillion', '%(value).1f nonillion', number),
        ungettext('%(value)s nonillion', '%(value)s nonillion', number),
    )),
    (33, lambda number: (
        ungettext('%(value).1f decillion', '%(value).1f decillion', number),
        ungettext('%(value)s decillion', '%(value)s decillion', number),
    )),
    (100, lambda number: (
        ungettext('%(value).1f googol', '%(value).1f googol', number),
        ungettext('%(value)s googol', '%(value)s googol', number),
    )),
)

@register.filter(is_safe=False)
def myintword(value):
    """
    Converts a large integer to a friendly text representation. Works best
    for numbers over 1 million. For example, 1000000 becomes '1.0 million',
    1200000 becomes '1.2 million' and '1200000000' becomes '1.2 billion'.
    """
    try:
        value = int(value)
    except (TypeError, ValueError):
        return value

    if value < 1000:
        return value

    def _check_for_i18n(value, float_formatted, string_formatted):
        """
        Use the i18n enabled defaultfilters.floatformat if possible
        """
        if settings.USE_L10N:
            value = defaultfilters.floatformat(value, 1)
            template = string_formatted
        else:
            template = float_formatted
        return template % {'value': value}

    for exponent, converters in intword_converters:
        large_number = 10 ** exponent
        if value < large_number * 1000:
            new_value = value / float(large_number)
            return _check_for_i18n(new_value, *converters(new_value))
    return value



@register.filter
def patfilter(value):
    if isinstance(value, str):
        return value
    template = ungettext('%(value)s time', '%(value)s times', value) 
    value = defaultfilters.floatformat(value, 1)
    return template % {'value' : value }

