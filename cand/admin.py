from django.contrib import admin
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response

from .models import Candidate, Property


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('candidate_name', 'candidate_sequential_nr', 'election_year', 
                   'post_description', 'party_acronym', 'candidate_electoral_id')
    list_filter = ('election_year',)

class PropertyAdmin(admin.ModelAdmin):
    list_display = ('candidate_sequential_nr', 'property_type_code',
                    'property_type_desc', 'property_desc', 'property_value')

admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Property, PropertyAdmin)
