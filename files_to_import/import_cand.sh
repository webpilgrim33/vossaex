#!/bin/bash

clear
read -p "Por favor, informe o ano dos arquivos de candidado para importar (Ex. 2006): " ano
echo ""
nome_arq=($(ls consulta_cand_$ano*))

for i in "${nome_arq[@]}"
do
    echo "Importando arquivo $i"
    /home/ubuntu/vossaex/project/manage.py import_candidates /home/ubuntu/vossaex/project/files_to_import/$i
done
