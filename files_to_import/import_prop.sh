#!/bin/bash

clear
read -p "Por favor, informe o ano dos arquivos de bens para importar (Ex. 2006): " ano
echo ""
nome_arq=($(ls bem_candidato_$ano*))

for i in "${nome_arq[@]}"
do
    echo "Importando arquivo $i"
    /home/ubuntu/vossaex/project/manage.py import_properties /home/ubuntu/vossaex/project/files_to_import/$i
done
