#!/bin/bash

clear
echo ""
nome_arq=($(ls consulta_cand_*))

source /home/ubuntu/vossaex/bin/activate

for i in "${nome_arq[@]}"
do
    echo "Importando arquivo $i"
    /home/ubuntu/vossaex/project/manage.py import_candidates /home/ubuntu/vossaex/project/files_to_import/$i
done
